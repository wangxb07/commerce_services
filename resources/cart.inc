<?php

/**
 * @file
 * Contains the resource callbacks for cart orders.
 */


/**
 * Returns a collection of cart orders for the current user.
 *
 * @see commerce_services_index_entities()
 */
function commerce_services_cart_index($fields, $expand_entities, $flatten_fields, $filter, $filter_op, $sort_by, $sort_order, $limit, $offset) {
  global $user;

  // For now, this resource only returns the current cart order of the given API
  // user. We can expand the collection to include all cart orders sorted so the
  // current order is first in the list at a later time.
  $uid = !empty($user->uid) ? $user->uid : 0;
  $order_id = commerce_cart_order_id($uid);

  if (!empty($order_id)) {
    $filter['order_id'] = $order_id;
    $filter_op['order_id'] = '=';

    $order = commerce_order_load($order_id);
    $order_state = commerce_order_state_load('checkout');

    if ($order->status === $order_state['default_status']) {
      $cart_state = commerce_order_state_load('cart');
      $order->status = $cart_state['default_status'];
      commerce_order_save($order);
    }
  }
  else {
    return array();
  }

  return commerce_services_index_entities('commerce_order', $fields, $expand_entities, $flatten_fields, $filter, $filter_op, $sort_by, $sort_order, $limit, $offset);
}

/**
 * Determines an API user's access to index cart orders.
 */
function commerce_services_cart_index_access() {
  // Because the entity access control system will filter any result sets,
  // there's no reason to limit a particular user's access.
  return TRUE;
}

/**
 * Creates a new cart order for the current API user.
 */
function commerce_services_cart_create($data, $flatten_fields) {
  global $user;
  $uid = !empty($user->uid) ? $user->uid : 0;

  // Even though the data array could technically accept a uid value, since the
  // resource is designed to work for the current API user this value will be
  // ignored and result in an error.
  if (isset($data['uid'])) {
    return services_error(t('Creating a cart does not permit you to set a uid value'), 400);
  }

  if (module_exists('commerce_stock')) {
    $qty = $data['quantity'];
    $product_id = $data['product_id'];
    $product = commerce_product_load($product_id);
    $qty_ordered = commerce_stock_check_cart_product_level($product_id);
    // Check using rules.
    commerce_stock_check_product_rule($product, $qty, $qty_ordered, $stock_state, $message);
    // Action.
    // TODO 库存错误的时候返回services error让前端去处理
    if ($stock_state == 1) {
      return services_error($message, 403);
    }
    //    elseif ($stock_state == 2) {
    //      drupal_set_message($message);
    //    }
  }

  // Create the new order with the customer's uid and the cart order status.
  // Instead of using commerce_cart_order_new() directly, its contents have been
  // copied here to allow for setting field and property data before saving so
  // any errors in the data prevent saving a new order.
  $order = commerce_order_new($uid, 'cart');
  $order->log = t('Created as a shopping cart order.');

  // Set the field and property data and save the new order.
  commerce_services_set_field_values('commerce_order', $order, $data, $flatten_fields);
  commerce_order_save($order);

  // Reset the cart cache
  commerce_cart_order_ids_reset();

  // If the user is not logged in, ensure the order ID is stored in the session.
  if (!$uid && empty($user->uid)) {
    commerce_cart_order_session_save($order->order_id);
  }

  // Add simplified fields to the order object for certain field types.
  commerce_services_decorate_entity('commerce_order', $order);

  // Flatten field value arrays if specified. This must be the last operation
  // performed as it breaks the standard field data model. An entity whose
  // fields have thus been flattened is no longer wrappable or writable.
  if ($flatten_fields == 'true') {
    $order = clone($order);
    commerce_services_flatten_fields('commerce_order', $order);
  }

  return $order;
}

/**
 * Determines an API user's access to create new cart orders.
 */
function commerce_services_cart_create_access() {
  // If the user has access to perform the operation...
  if (commerce_order_access('create')) {
    return TRUE;
  }
  else {
    return services_error(t('Access to this operation not granted'), 401);
  }
}

/**
 * Add product to current cart
 */
function commerce_services_cart_add_product($data, $flatten_fields) {
  global $user;

  $product_id = $data['product_id'];
  $quantity = $data['quantity'];

  unset($data['quantity']);
  unset($data['product_id']);

  if (!is_numeric($quantity) || $quantity <= 0) {
    return services_error(t('You must specify a valid quantity to add to the cart.'), 400);
  }

  if ((int)$quantity != $quantity) {
    return services_error(t('You must specify a whole number for the quantity.'), 400);
  }

  $product = commerce_product_load($product_id);

  if (!$product) {
    return services_error(t('Not found product.'), 400);
  }

  if (module_exists('commerce_stock')) {
    $product = commerce_product_load($product_id);
    $qty_ordered = commerce_stock_check_cart_product_level($product_id);
    // Check using rules.
    commerce_stock_check_product_rule($product, $quantity, $qty_ordered, $stock_state, $message);
    // Action.
    // TODO 库存错误的时候返回services error让前端去处理
    if ($stock_state == 1) {
      return services_error($message, 403);
    }
    //    elseif ($stock_state == 2) {
    //      drupal_set_message($message);
    //    }
  }

  if ($user->uid) {
    $line_item = commerce_product_line_item_new($product, $quantity, 0, $data, 'product');
    unset($data['context']);

    drupal_alter('commerce_product_calculate_sell_price_line_item', $line_item);

    commerce_product_pricing_invoke($line_item);

    commerce_services_set_field_values('commerce_line_item', $line_item, $data, $flatten_fields);

    // Only attempt an Add to Cart if the line item has a valid unit price.
    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

    if (!is_null($line_item_wrapper->commerce_unit_price->value())) {
      // Add the product to the specified shopping cart.
      return commerce_cart_product_add(
        $user->uid,
        $line_item,
        isset($line_item->data['context']['add_to_cart_combine']) ? $line_item->data['context']['add_to_cart_combine'] : TRUE
      );
    }
    else {
      return services_error(t('%title could not be added to your cart.', array('%title' => $product->title)), 400);
    }
  }
  else {
    return services_error(t('Not granted add to cart operation for anonymous user'), 400);
  }
}

/**
 * Determines an API user's access to add product.
 */
function commerce_services_cart_add_product_access() {
  if (user_access('access checkout')) {
    return TRUE;
  }
  else {
    return services_error(t('Access to this operation not granted'), 401);
  }
}

/**
 * Delete product line item in current cart order
 */
function commerce_services_cart_delete_product($order_id, $line_item_id) {
  $order = commerce_order_load($order_id);

  commerce_cart_order_product_line_item_delete($order, $line_item_id);

  return commerce_services_retrieve_entity('commerce_order', $order_id, TRUE, TRUE);
}

/**
 * Determines an API user's access to delete product line item.
 */
function commerce_services_cart_delete_product_access($order_id, $line_item_id) {
  global $user;

  if ($line_item = commerce_line_item_load($line_item_id)) {
    if ($order = commerce_order_load($order_id)) {
      if ($order->uid != $user->uid) {
        return services_error(t('This order does not belong to this user'), 401);
      }
    }
    else {
      return services_error(t('Order not found'), 404);
    }
  }
  else {
    return services_error(t('Line item not found'), 404);
  }

  return TRUE;
}

/**
 * Updates a line item in cart order.
 */
function commerce_services_cart_update_product($line_item_id, $data, $flatten_fields) {
  // Only allow update line item quantity
  $new_data = array();
  $new_data['quantity'] = $data['quantity'];

  ctools_include('line_item', 'commerce_services', 'resources');

  $line_item = commerce_services_line_item_update($line_item_id, $new_data, $flatten_fields);

  return commerce_services_retrieve_entity('commerce_order', $line_item->order_id, TRUE, TRUE);
}

/**
 * Determines an API user's access to delete product line item.
 */
function commerce_services_cart_update_product_access($line_item_id) {
  global $user;

  if ($line_item = commerce_line_item_load($line_item_id)) {
    if ($order = commerce_order_load($line_item->order_id)) {
      if ($order->uid != $user->uid) {
        return services_error(t('This order does not belong to this user'), 401);
      }
    }
    else {
      return services_error(t('Order not found'), 404);
    }
  }
  else {
    return services_error(t('Line item not found'), 404);
  }

  return TRUE;
}
