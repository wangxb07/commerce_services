<?php

/**
 * @file
 * Contains the resource callbacks for customer.
 */

/**
 * Returns a collection of customers.
 *
 * @see commerce_services_index_entities()
 */
function commerce_services_customer_index($fields, $expand_entities, $flatten_fields, $filter, $filter_op, $sort_by, $sort_order, $limit, $offset) {
  return commerce_services_index_entities('commerce_customer_profile', $fields, $expand_entities, $flatten_fields, $filter, $filter_op, $sort_by, $sort_order, $limit, $offset);
}

/**
 * Determines an API user's access to index orders.
 */
function commerce_services_customer_index_access() {
  return TRUE;
}

/**
 * Returns a single customer profile.
 *
 * @see commerce_services_retrieve_entity()
 */
function commerce_services_customer_retrieve($profile_id, $expand_entities, $flatten_fields) {
  return commerce_services_retrieve_entity('commerce_customer_profile', $profile_id, $expand_entities, $flatten_fields);
}

/**
 * Determines an API user's access to retrieve a given customer profile.
 */
function commerce_services_customer_retrieve_access($profile_id) {
  $profile = commerce_customer_profile_load($profile_id);

  if ($profile) {
    if (commerce_customer_profile_access('view', $profile)) {
      return TRUE;
    }
    else {
      return services_error(t('Access to this operation not granted'), 401);
    }
  }
  else {
    return services_error(t('Customer profile not found'), 404);
  }
}

/**
 * Creates a new customer profile.
 */
function commerce_services_customer_create($data, $flatten_fields) {
  global $user;
  // Ensure the create request specifies a valid customer type.
  if (empty($data['type']) || !in_array($data['type'], array_keys(commerce_customer_profile_types()))) {
    return services_error(t('You must specify a valid customer type'), 400);
  }

  $profile = commerce_customer_profile_new($data['type'], $user->uid);

  commerce_services_set_field_values('commerce_customer_profile', $profile, $data, $flatten_fields);
  commerce_customer_profile_save($profile);

  if ($flatten_fields == 'true') {
    $profile = clone($profile);
    commerce_services_flatten_fields('commerce_customer_profile', $profile);
  }

  return $profile;
}

/**
 * Determines an API user's access to create new customer profile.
 */
function commerce_services_customer_create_access() {
  if (commerce_customer_profile_access('create')) {
    return TRUE;
  }
  else {
    return services_error(t('Access to this operation not granted'), 401);
  }
}

/**
 * Updates a customer profile.
 */
function commerce_services_customer_update($profile_id, $data, $flatten_fields) {
  return commerce_services_update_entity('commerce_customer_profile', $profile_id, $data, $flatten_fields);
}

/**
 * Determines an API user's access to update a given customer profile.
 */
function commerce_services_customer_update_access($profile_id) {
  $profile = commerce_customer_profile_load($profile_id);

  if ($profile) {
    if (commerce_customer_profile_access('update', $profile)) {
      return TRUE;
    }
    else {
      return services_error(t('Access to this operation not granted'), 401);
    }
  }
  else {
    return services_error(t('Customer profile not found'), 404);
  }
}
