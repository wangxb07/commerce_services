<?php

/**
 * @file
 * Contains the resource callbacks for orders.
 */


/**
 * Returns a collection of orders.
 *
 * @see commerce_services_index_entities()
 */
function commerce_services_order_index($fields, $expand_entities, $flatten_fields, $filter, $filter_op, $sort_by, $sort_order, $limit, $offset) {
  $results = commerce_services_index_entities('commerce_order', $fields, $expand_entities, $flatten_fields, $filter, $filter_op, $sort_by, $sort_order, $limit, $offset);
  return array_values($results);
}

/**
 * Determines an API user's access to index orders.
 */
function commerce_services_order_index_access() {
  // Because the entity access control system will filter any result sets,
  // there's no reason to limit a particular user's access.
  return TRUE;
}

/**
 * Returns a single order.
 *
 * @see commerce_services_retrieve_entity()
 */
function commerce_services_order_retrieve($order_id, $expand_entities, $flatten_fields) {
  return commerce_services_retrieve_entity('commerce_order', $order_id, $expand_entities, $flatten_fields);
}

/**
 * Determines an API user's access to retrieve a given order.
 *
 * @param $order_id
 *   The ID of the order to be retrieved.
 *
 * @return
 *   Boolean indicating the user's access to retrieve the order.
 */
function commerce_services_order_retrieve_access($order_id) {
  // Attempt to load the order.
  if ($order = commerce_order_load($order_id)) {
    // And perform the view access check.
    if (commerce_order_access('view', $order)) {
      return TRUE;
    }
    else {
      return services_error(t('Access to this operation not granted'), 401);
    }
  }
  else {
    return services_error(t('Order not found'), 404);
  }
}

/**
 * Updates an order.
 *
 * @see commerce_services_update_entity()
 */
function commerce_services_order_update($order_id, $data, $flatten_fields) {
  return commerce_services_update_entity('commerce_order', $order_id, $data, $flatten_fields);
}

/**
 * Determines an API user's access to update a given order.
 *
 * @param $order_id
 *   The ID of the order to be updated.
 *
 * @return
 *   Boolean indicating the user's access to update the order.
 */
function commerce_services_order_update_access($order_id) {
  // Attempt to load the order.
  if ($order = commerce_order_load($order_id)) {
    // If the user has access to perform the operation...
    if (commerce_order_access('update', $order)) {
      return TRUE;
    }
    else {
      return services_error(t('Access to this operation not granted'), 401);
    }
  }
  else {
    return services_error(t('Order not found'), 404);
  }
}

/**
 * Deletes an order.
 *
 * @param $order_id
 *   The ID of the order to be deleted.
 */
function commerce_services_order_delete($order_id) {
  commerce_order_delete($order_id);
}

/**
 * Determines an API user's access to delete a given order.
 *
 * @param $order_id
 *   The ID of the order to be deleted.
 *
 * @return
 *   Boolean indicating the user's access to delete the order.
 */
function commerce_services_order_delete_access($order_id) {
  // Attempt to load the order.
  if ($order = commerce_order_load($order_id)) {
    // If the user has access to perform the operation...
    if (commerce_order_access('delete', $order)) {
      return TRUE;
    }
    else {
      return services_error(t('Access to this operation not granted'), 401);
    }
  }
  else {
    return services_error(t('Order not found'), 404);
  }
}

/**
 * Returns a collection of line items on an order.
 *
 * @see commerce_services_index_entities()
 */
function commerce_services_order_line_item_index($order_id, $fields, $expand_entities, $flatten_fields, $filter, $filter_op, $sort_by, $sort_order, $limit, $offset) {
  // Add the order ID to the filter list.
  $filter['order_id'] = $order_id;
  $filter_op['order_id'] = '=';

  return commerce_services_index_entities('commerce_line_item', $fields, $expand_entities, $flatten_fields, $filter, $filter_op, $sort_by, $sort_order, $limit, $offset);
}

/**
 * Determines an API user's access to index line items on an order.
 */
function commerce_services_order_line_item_index_access($order_id) {
  // Attempt to load the order.
  if ($order = commerce_order_load($order_id)) {
    // And perform the view access check.
    if (commerce_order_access('view', $order)) {
      return TRUE;
    }
    else {
      return services_error(t('Access to this operation not granted'), 401);
    }
  }
  else {
    return services_error(t('Order not found'), 404);
  }
}

/**
 * Update order state to checkout default status
 *
 * @param $order_id
 *   The ID of the order to be deleted.
 *
 * @param $data
 *   The order data for data field to update
 *
 * @return
 *   success status is TRUE
 */
function commerce_services_order_checkout($order_id, $data) {
  $order = commerce_order_load($order_id);

  // Set the order status to the first checkout page's status.
  $order_state = commerce_order_state_load('checkout');
  $new_order = commerce_order_status_update($order, $order_state['default_status'], TRUE);

  if (!empty($data)) {
    $new_order->data = array_merge($order->data, $data);
  }

  // Because of the commerce_order_partial strategy, the quantity of products that need
  // to be saved first is set to 0 before shipping rates init.
  if (module_exists('commerce_order_partial')) {
    commerce_order_partial_to_checkout($order);
  }

  _commerce_services_order_checkout_shipping_init($new_order);

  // Skip saving in the status update and manually save here to force a save
  // even when the status doesn't actually change.
  if (variable_get('commerce_order_auto_revision', TRUE)) {
    $new_order->revision = TRUE;
    $new_order->log = t('Customer proceeded to checkout using services.');
  }

  commerce_order_save($new_order);

  return (object)array(
    'status' => TRUE
  );
}

/**
 * Creates a new checkout order for the current API user.
 */
function commerce_services_order_checkout_create($data, $flatten_fields, $init_status = 'checkout_payment') {
  global $user;

  // Even though the data array could technically accept a uid value, since the
  // resource is designed to work for the current API user this value will be
  // ignored and result in an error.
  if (isset($data['uid'])) {
    return services_error(t('Creating a cart does not permit you to set a uid value'), 400);
  }

  if (module_exists('commerce_stock')) {
    $qty = $data['quantity'];
    $product_id = $data['product_id'];
    $product = commerce_product_load($product_id);
    $qty_ordered = commerce_stock_check_cart_product_level($product_id);
    // Check using rules.
    commerce_stock_check_product_rule($product, $qty, $qty_ordered, $stock_state, $message);
    // Action.
    // TODO 库存错误的时候返回services error让前端去处理
    if ($stock_state == 1) {
      return services_error($message, 403);
    }
    // elseif ($stock_state == 2) {
    //   services_error($message);
    // }
  }

  // product line item add
  $product_id = $data['product_id'];
  $quantity = $data['quantity'];

  unset($data['quantity']);
  unset($data['product_id']);

  $context = array();

  if (isset($data['context'])) {
    $context = $data['context'];
    unset($data['context']);
  }

  if (!is_numeric($quantity) || $quantity <= 0) {
    return services_error(t('You must specify a valid quantity to add to the cart.'), 400);
  }

  if ((int)$quantity != $quantity) {
    return services_error(t('You must specify a whole number for the quantity.'), 400);
  }

  $product = commerce_product_load($product_id);
  if (!$product) {
    return services_error(t('Not found product.'), 400);
  }

  // create new empty order
  $order = commerce_order_new($user->uid, $init_status);
  $order->log = t('Created as a checkout order by order checkout-create services action.');

  $order->data['context'] = $context;

  // first time save the order
  commerce_order_save($order);

  // product line item add to order
  $line_item = commerce_product_line_item_new($product, $quantity, 0, $data['line_item'], 'product');
  unset($data['line_item']['context']);

  // alter hook invoke
  drupal_alter('commerce_product_calculate_sell_price_line_item', $line_item);

  // rule event invoke
  commerce_product_pricing_invoke($line_item);

  // set the line item field values
  commerce_services_set_field_values('commerce_line_item', $line_item, $data['line_item'], $flatten_fields);
  unset($data['line_item']);

  if (_commerce_services_order_product_add($order, $line_item) === FALSE) {
    return services_error(t('%title could not be added to your cart.', array('%title' => $product->title)), 400);
  }

  // Set the field and property data and save the new order.
  if (!empty($data)) {
    commerce_services_set_field_values('commerce_order', $order, $data, $flatten_fields);
  }

  // init shipping line item
  _commerce_services_order_checkout_shipping_init($order);

  // finial save the order
  commerce_order_save($order);

  // Add simplified fields to the order object for certain field types.
  commerce_services_decorate_entity('commerce_order', $order);

  // Flatten field value arrays if specified. This must be the last operation
  // performed as it breaks the standard field data model. An entity whose
  // fields have thus been flattened is no longer wrappable or writable.
  if ($flatten_fields == 'true') {
    $order = clone($order);
    commerce_services_flatten_fields('commerce_order', $order);
  }

  return $order;
}

/**
 * Determines an API user's access to create checkout order.
 */
function commerce_services_order_checkout_create_access() {
  if(user_access('access checkout')) {
    return TRUE;
  }
  else {
    return services_error(t('Access to this operation not granted'), 401);
  }
}

/**
 * Determines an API user's access to checkout cart order.
 */
function commerce_services_order_checkout_access($order_id) {
  // Attempt to load the order.
  if ($order = commerce_order_load($order_id)) {
    // And perform the view access check.
    if (commerce_checkout_access($order)) {
      return TRUE;
    }
    else {
      return services_error(t('Access to this operation not granted'), 401);
    }
  }
  else {
    return services_error(t('Order not found'), 404);
  }
}

/**
 * Update order information in checkout status
 *
 * @param $order_id
 *   The ID of the order to be deleted.
 */
function commerce_services_order_checkout_update($order_id, $data, $flatten_fields, $expand_entities) {
  unset($data['commerce_line_items']);
  unset($data['commerce_order_total']);
  unset($data['uid']);
  unset($data['type']);

  $recalculate_shipping = FALSE;
  if (isset($data['recalculate_shipping']) && $data['recalculate_shipping']) {
    $recalculate_shipping = TRUE;
    unset($data['recalculate_shipping']);
  }

  $shipping_service = FALSE;
  if (isset($data['shipping_service'])) {
    $shipping_service = $data['shipping_service'];
    unset($data['shipping_service']);
  }

  $attach_to_data = FALSE;
  if (isset($data['attach_to_data'])) {
    $attach_to_data = $data['attach_to_data'];
    unset($data['attach_to_data']);
  }

  $order = commerce_order_load($order_id);
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Set field values using the data provided.
  commerce_services_set_field_values('commerce_order', $order, $data, $flatten_fields);

  // recalculate shipping
  if ($recalculate_shipping) {
    _commerce_services_order_checkout_shipping_init($order);
  }
  elseif ($shipping_service) {
    commerce_shipping_collect_rates($order);

    if (empty($order->shipping_rates) || empty($order->shipping_rates[$shipping_service])) {
      return FALSE;
    }

    // delete old shipping line items
    commerce_shipping_delete_shipping_line_items($order, TRUE);
    $rate_line_item = $order->shipping_rates[$shipping_service];

    // create new shipping line item
    $rate_line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $rate_line_item);
    $unit_price = $rate_line_item_wrapper->commerce_unit_price->value();

    // Create a new shipping line item with the calculated rate from the form.
    $line_item = commerce_shipping_line_item_new($shipping_service, $unit_price, $order->order_id, $rate_line_item->data, $rate_line_item->type);

    // Save and add the line item to the order.
    commerce_shipping_add_shipping_line_item($line_item, $order, TRUE);
  }

  if ($attach_to_data) {
    $order->data = array_merge($order->data, $attach_to_data);
  }

  // order refresh
  commerce_cart_order_refresh($order_wrapper);

  // Save the updates to the order
  commerce_order_save($order);

  // Add simplified fields to the entity object for certain field types.
  commerce_services_decorate_entity('commerce_order', $order);

  // Expand referenced entities into the full entity to the specified depth.
  if ($expand_entities > 0) {
    commerce_services_expand_entities('commerce_order', $order, $expand_entities, $flatten_fields == 'true');
  }

  if ($flatten_fields == 'true') {
    $order = clone($order);
    commerce_services_flatten_fields('commerce_order', $order);
  }

  return $order;
}

/**
 * Determines an API user's access to update order information in checkout status
 */
function commerce_services_order_checkout_update_access($order_id) {
  if ($order = commerce_order_load($order_id)) {
    if(user_access('access checkout')) {
      return TRUE;
    }
    else {
      return services_error(t('Access to this operation not granted'), 401);
    }

    if ($order->status !== 'checkout_checkout') {
      return services_error(t('Order status is not checkout'), 401);
    }
  }
  else {
    return services_error(t('Order not found'), 404);
  }
}

/**
 * Determines an API user's access to shipment order
 * @throws ServicesException
 */
function commerce_services_order_shipment_access($order_id) {
  if ($order = commerce_order_load($order_id)) {
    if(user_access('access checkout')) {
      return TRUE;
    }
    else {
      return services_error(t('Access to this operation not granted'), 401);
    }

    if ($order->state !== 'pending') {
      return services_error(t('Order state is not pending'), 401);
    }
  }
  else {
    return services_error(t('Order not found'), 404);
  }
}

/**
 * Add shipping information and update the status of the order to shipped
 *
 * @param $order_id
 * @param $data
 * @param $flatten_fields
 * @param $expand_entities
 */
function commerce_services_order_shipment($order_id, $data, $flatten_fields, $expand_entities) {
  $order = commerce_order_load($order_id);
  if (property_exists($order, 'field_logistics_info') && !empty($data['logistic_no'])) {
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $order_wrapper->field_logistics_info->set($data['logistic_no']);
    $order_wrapper->status->set('processing');

    $order_wrapper->save();
  }

  // Add simplified fields to the entity object for certain field types.
  commerce_services_decorate_entity('commerce_order', $order);

  // Expand referenced entities into the full entity to the specified depth.
  if ($expand_entities > 0) {
    commerce_services_expand_entities('commerce_order', $order, $expand_entities, $flatten_fields == 'true');
  }

  if ($flatten_fields == 'true') {
    $order = clone($order);
    commerce_services_flatten_fields('commerce_order', $order);
  }

  return $order;
}

/**
 * Helper function for checkout process shipping line item initialize
 */
function _commerce_services_order_checkout_shipping_init(&$order) {
  if (module_exists('commerce_shipping')) {
    // Delete any existing shipping line items from the order.
    commerce_shipping_delete_shipping_line_items($order, TRUE);
    // add shipping line item to new checkout order
    commerce_shipping_collect_rates($order);

    if (empty($order->shipping_rates)) {
      unset($order->data['shipping_rates']);
      return FALSE;
    }

    $default_shipping_method = variable_get('commerce_services_default_shipping_method', NULL);

    $shipping_services = commerce_shipping_services($default_shipping_method);
    $default_shipping_service = current(array_keys($shipping_services));

    // Extract the unit price from the calculated rate.
    if (isset($order->shipping_rates[$default_shipping_service])) {
      $rate_line_item = $order->shipping_rates[$default_shipping_service];
    }
    elseif (count($order->shipping_rates) > 0) {
      $rate_line_item = current($order->shipping_rates);
    }
    else {
      return FALSE;
    }

    $order->data['shipping_rates'] = $order->shipping_rates;

    $rate_line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $rate_line_item);
    $unit_price = $rate_line_item_wrapper->commerce_unit_price->value();

    // Create a new shipping line item with the calculated rate from the form.
    $line_item = commerce_shipping_line_item_new($rate_line_item->data['shipping_service']['name'], $unit_price, $order->order_id, $rate_line_item->data, $rate_line_item->type);

    // Save and add the line item to the order.
    commerce_shipping_add_shipping_line_item($line_item, $order, TRUE);
  }
}

/**
 * Adds the specified product to a specified order.
 */
function _commerce_services_order_product_add($order, $line_item, $skip_save = TRUE) {
  // Do not add the line item if it doesn't have a unit price.
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  if (is_null($line_item_wrapper->commerce_unit_price->value())) {
    return FALSE;
  }

  // Set the incoming line item's order_id.
  $line_item->order_id = $order->order_id;

  // Wrap the order for easy access to field data.
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Extract the product and quantity we're adding from the incoming line item.
  $product = $line_item_wrapper->commerce_product->value();
  $quantity = $line_item->quantity;

  // Save the incoming line item now so we get its ID.
  commerce_line_item_save($line_item);

  // Add it to the order's line item reference value.
  $order_wrapper->commerce_line_items[] = $line_item;

  // Save the updated order.
  if(!$skip_save) {
    commerce_order_save($order);
  }

  // Return the line item.
  return $line_item;
}
