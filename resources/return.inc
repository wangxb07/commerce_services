<?php

/**
 * @file
 * Contains the resource callbacks for commerce return entity.
 */

/**
 * Returns a collection of returns.
 *
 * @see commerce_services_index_entities()
 */
function commerce_services_return_index($fields, $expand_entities, $flatten_fields, $filter, $filter_op, $sort_by, $sort_order, $limit, $offset) {
  return commerce_services_index_entities('commerce_return', $fields, $expand_entities, $flatten_fields, $filter, $filter_op, $sort_by, $sort_order, $limit, $offset);
}

/**
 * Determines an API user's access to index returns.
 */
function commerce_services_return_index_access() {
  return TRUE;
}

/**
 * Creates a new return entity for the current API user.
 */
function commerce_services_return_create($order_id, $data, $flatten_fields) {
  $order = commerce_order_load($order_id);

  $return_entity = commerce_return_new($order, $order->uid);
  $return_wrapper = entity_metadata_wrapper('commerce_return', $return_entity);

  // Hold the return line item quantity, method and reason, That will be saved in the data attribute of return line item
  //
  // array(
  //   quantity => integer,
  //   product_id => integer,
  //   commerce_return => array(
  //     reason_tid => integer,
  //     method_tid => integer,
  //     notes => string,
  //     line_item_status => string
  //   )
  // )
  $commerce_return_data = $data['return_line_items'];

  // Default the line_item_key is product_id
  foreach ($commerce_return_data as $item_data) {
    foreach ($return_wrapper->commerce_return_line_items as $line_item_wrapper) {
      $product_id = $line_item_wrapper->commerce_product->raw();
      $line_item = $line_item_wrapper->value();

      if ($product_id == $item_data['product_id']) {
        // set quantity
        $line_item_wrapper->quantity->set($item_data['quantity']);

        // set data
        $line_item->data = array_merge($line_item->data, array('commerce_return' => $item_data['commerce_return']));

        commerce_line_item_save($line_item);
        continue;
      }
    }
  }

  unset($data['return_line_items']);

  commerce_services_set_field_values('commerce_return', $return_entity, $data, $flatten_fields);

  commerce_return_save($return_entity);

  // Flatten field value arrays if specified. This must be the last operation
  // performed as it breaks the standard field data model. An entity whose
  // fields have thus been flattened is no longer wrappable or writable.
  if ($flatten_fields == 'true') {
    $return_entity = clone($return_entity);
    commerce_services_flatten_fields('commerce_return', $return_entity);
  }

  return $return_entity;
}

/**
 * Determines an API user's access to create commerce return entity.
 */
function commerce_services_return_create_access($order_id) {
  global $user;

  if(user_access('access return of goods')) {
    if ($order = commerce_order_load($order_id)) {
      if ($order->uid != $user->uid) {
        return services_error(t('The owner of the order does not match.'), 401);
      }
    }
    else {
      return services_error(t('Order not found'), 404);
    }

    return TRUE;
  }
  else {
    return services_error(t('Access to this operation not granted'), 401);
  }
}

/**
 * Returns all reason of commerce return line item.
 */
function commerce_services_return_reason_index() {
  $reasons = commerce_return_reasons();

  return array_values($reasons);
}

/**
 * Determines an API user's access to reason index returns.
 */
function commerce_services_return_reason_index_access() {
  return TRUE;
}

/**
 * Returns all method of commerce return line item.
 */
function commerce_services_return_method_index() {
  $methods = commerce_return_methods();

  return array_values($methods);
}

/**
 * Determines an API user's access to method index returns.
 */
function commerce_services_return_method_index_access() {
  return TRUE;
}