<?php

/**
 * @file
 * Contains the resource callbacks for discount.
 */

/**
 * Returns a collection of discounts.
 *
 * commerce_discount is not a common entity type, so can't use common helper function to return the index
 */
function commerce_services_discount_index($type, $limit, $offset, $date_limit_only, $date_limit, $expand_products) {
  if (!in_array($type, array_keys(commerce_discount_types()))) {
    return services_error(t('Unknown discount type @type', array('@type' => $type)), 400);
  }

  $entity_type = 'commerce_discount';
  $entity_info = entity_get_info($entity_type);

  // Build a query to load all accessible entities.
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', $entity_type)
    ->propertyCondition('type', $type)
    ->propertyCondition('status', TRUE)
    ->range($offset, $limit);

  if (is_array($date_limit) && count($date_limit) === 2) {
    $query->fieldCondition('commerce_discount_date', 'value', $date_limit[0], ">=", 0);
    $query->fieldCondition('commerce_discount_date', 'value2', $date_limit[1], "<=", 0);
  }

  // Add the access query tag if specified for the entity type.
  if (!empty($entity_info['access arguments']['access tag'])) {
    $query->addTag($entity_info['access arguments']['access tag']);
  }

  // Execute the query and load the entities it returns if any.
  $result = $query->execute();
  $entities = array();

  if (!empty($result[$entity_type])) {
    $entities = entity_load($entity_type, array_keys($result[$entity_type]));

    foreach ($entities as $entity_id => &$entity) {
      // offer expand

      $entity_wrap = entity_metadata_wrapper('commerce_discount', $entity);
      $offer = $entity_wrap->commerce_discount_offer->value();
      $offer = clone($offer);
      commerce_services_flatten_fields('commerce_discount_offer', $offer);
      $entity->offer = $offer;

      if ($expand_products == 'true') {
        $products = array();

        foreach ($entity->inline_conditions as $langcode => $items) {
          if ($langcode != LANGUAGE_NONE && !in_array($langcode, array_keys(language_list()))) {
            return services_error(t('Unknown language @langcode in inline conditions', array('@langcode' => $langcode)), 400);
          }
          elseif (!is_array($items)) {
            return services_error(t('Invalid inline conditions value given'), 400);
          }

          foreach ($items as $delta => $item) {
            if (empty($item)) {
              unset($entity->inline_conditions[$langcode][$delta]);
            }
            else {
              // need check to product access
              $products_display = array();
              if ($item['condition_name'] == 'commerce_product_contains_products') {
                // multiple skus are allowed in one inline condition
                foreach($item['condition_settings']['sku'] as $val) {
                  $products_display[] = _commerce_services_discount_referenced_product_display($val['product_id']);
                }
              }
              if ($item['condition_name'] == 'commerce_order_has_specific_quantity_products') {
                // multiple skus are allowed in one inline condition
                foreach($item['condition_settings']['products'] as $val) {
                  $products_display[] = _commerce_services_discount_referenced_product_display($val['product_id']);
                }
              }

              foreach ($products_display as &$product) {
                if ($product) {
                  foreach ($product as $p) {
                    // Add simplified fields to the entity object for certain field types.
                    commerce_services_decorate_entity('node', $p);
                    commerce_services_expand_entities('node', $p, 1, true, 1);
                    $p = clone($p);
                    commerce_services_flatten_fields('node', $p);

                    if ($item['condition_name'] == 'commerce_order_has_specific_quantity_products') {
                      $p->condition_quantity = $item['condition_settings']['quantity'];
                      $p->condition_operator = $item['condition_settings']['operator'];
                    }
                    $products[$p->nid] = $p;
                  }
                }
              }
            }
          }
        }

        $entity->condition_products = $products;
      }

      commerce_services_flatten_fields('commerce_discount', $entity);
    }
  }

  return $entities;
}

/**
 * Retrieve the referenced produdct display by product
 */
function _commerce_services_discount_referenced_product_display($product_id) {
  $query = new EntityFieldQuery();

  $query->addTag('node_access')
    ->addMetaData('op', 'view')
    ->addMetaData('base_table', 'node')
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', array_keys(commerce_product_reference_node_types()))
    ->fieldCondition('field_product', 'product_id', $product_id);

  $result = $query->execute();

  if (!empty($result['node'])) {
    return entity_load('node', array_keys($result['node']));
  }
  else {
    return FALSE;
  }
}

/**
 * Determines an API user's access to index discounts.
 *
 * @return bool
 */
function commerce_services_discount_index_access() {
  return TRUE;
}
